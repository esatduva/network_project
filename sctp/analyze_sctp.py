import sys


def is_number(string):
	try:
		float(string)
		return True
	except ValueError:
		return False

nam_file = open(sys.argv[1])

line=0;
send=0;
receive=0;
hop=0;
send=0

starttime = 0;
stoptime = 0;
byte = 0;
count = 0;

receivetime = {}
sendtime = {}

for line in nam_file:
	
	line_args = line.split(' ')
	
	if( is_number( line_args[2] ) ):
		
		#start time
		if( count == 0 ):
			starttime = float(line_args[2])

		#stop time 
		if( float(line_args[2]) > stoptime  ):
			stoptime = float(line_args[2])

		#recieved
		if( line_args[0] == 'r' and line_args[6] == '4' and line_args[4] != '3'):
			
			receive+=1
			byte+=float(line_args[10])
			receivetime[line_args[14]]=float(line_args[2])

		#hop
		if( line_args[0] == 'h' ):
			hop+=1
			if( line_args[4] == '1' or line_args[4]  == '2' ):
				send+=1
				sendtime[line_args[14]]=float(line_args[2])

	count+=1


delay = 0
s = 0
differences = {}

for time in sendtime:
		
	if time in receivetime:
		delay+=receivetime[time]-sendtime[time]
		differences[s]=delay
		s+=1

#jitter
totaldiff = 0
jittersample = 0
for i in range(0,s-1):
	totaldiff += differences[i+1] - differences[i]
	jittersample+=1

throughput = (byte / (stoptime-starttime ) ) * 8 / 1024






output = ''

output+= "\n==========================================================="	
output+= "\n=====================Nam Analyzer=========================="
output+= "\n==========================================================="

output+= "\nRunning Time : "+str(stoptime-starttime)
output+= "\nPacket Sent:"+str(send)
output+= "\nHop:"+str(hop)
output+= "\nPacket Received:"+str(receive)
output+= "\nDropped Packets:"+str(send-receive)
output+= "\nTotal bytes:"+str(byte)+" byte"
output+= "\nThroughput : "+str(throughput)+" kbps"
output+= "\nEnd to End Delay:"+str(delay/s)
output+= "\nJitter :"+str(totaldiff/jittersample)
output+= "\n============================================================\n"

print output

if len(sys.argv)>2:
	file_output = open(sys.argv[2],'w')
	file_output.write(output)
	file_output.close()



