# EXAMPLE SCRIPT 3
#
# Demonstrates multihoming. One endpoint is single homed while the other
# is multihomed. They are connected through a router. Shows that the they
# can be combined. The sender is multihomed. The sender has the HEARTBEAT
# mechanism turned off. In the middle of the association, the sender does
# a "force-source", which means that the sender forces the source
# interface used for outgoing traffic.
#
#       host0_if0      O===
#                     /    \\
#       host0_core   O       O=======O  host1
#                     \    //
#       host0_if1      O===  

Trace set show_sctphdr_ 1

set ns [new Simulator]
set nf [open sctp.nam w]
$ns namtrace-all $nf

#set allchan [open all.tr w]
#$ns trace-all $allchan

proc finish {} {
    global ns nf allchan

    set PERL "/usr/bin/perl"
    set USERHOME [exec env | grep "^HOME" | sed /^HOME=/s/^HOME=//]
    set NSHOME "$USERHOME/ns-allinone-2.35"
    set XGRAPH "$NSHOME/bin/xgraph"
    set SETFID "$NSHOME/ns-2.35/bin/set_flow_id"
    set RAW2XG_SCTP "$NSHOME/ns-2.35/bin/raw2xg-sctp"
    set GETRC "$NSHOME/ns-2.35/bin/getrc"

    $ns flush-trace
    close $nf
    #close $allchan

    exec nam sctp.nam &

    exit 0
}

set host0_core [$ns node]
set host0_if0 [$ns node]
set host0_if1 [$ns node]
$host0_core color Red
$host0_if0 color Red
$host0_if1 color Red
$ns multihome-add-interface $host0_core $host0_if0
$ns multihome-add-interface $host0_core $host0_if1

set host1 [$ns node]
$host1 color Blue

set router [$ns node]

$ns duplex-link $host0_if0 $router 3.0Mb 100ms DropTail
$ns duplex-link $host0_if1 $router 3.0Mb 100ms DropTail

$ns duplex-link $host1 $router .5Mb 100ms DropTail

$ns queue-limit $router $host1 5
set sctp0 [new Agent/SCTP]
$ns multihome-attach-agent $host0_core $sctp0
$sctp0 set fid_ 0 
$sctp0 set debugMask_ -1
$sctp0 set debugFileIndex_ 0
$sctp0 set mtu_ 1500
$sctp0 set dataChunkSize_ 1468
$sctp0 set numOutStreams_ 1
$sctp0 set oneHeartbeatTimer_ 0   # turns off heartbeating

set sctp1 [new Agent/SCTP]
$ns attach-agent $host1 $sctp1
$sctp1 set debugMask_ -1
$sctp1 set debugFileIndex_ 1
$sctp1 set mtu_ 1500
$sctp1 set initialRwnd_ 131072 
$sctp1 set useDelayedSacks_ 1

$ns color 0 Red
$ns color 1 Blue

$ns connect $sctp0 $sctp1

set ftp0 [new Application/FTP]
$ftp0 attach-agent $sctp0

# force the source interface
$ns at 6.0 "$sctp0 force-source $host0_if1"

$ns at 0.5 "$ftp0 start"
$ns at 10.0 "finish"

$ns run

